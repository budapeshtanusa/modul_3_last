#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#define LAST_MESSAGE 255

struct mymsgbuf
{
	long mtype;
	int id;
	char message[100];
}mybuf;

int main() {
	int msqid,maxlen,i,len,pid;
	char pathname[]="client.c";
	key_t key;
	if((key=ftok(pathname,0))<0) {
		printf("Error: can't generate key\n");
		exit(-1);
	}
	if((msqid=msgget(key, 0666 | IPC_CREAT))<0) {
		printf("Error: can't get msqid\n");
		exit(-1);
	}
	pid=fork();
	if(-1==pid) {
		printf("Error: can't fork.\n");
		exit(-1);
	}
	else if(0==pid){
		mybuf.mtype=1;
		mybuf.id=getpid();
		len=sizeof(mybuf);
		if(msgsnd(msqid,(struct msgbuf*)&mybuf,len,0)<0) {
			printf("Error: Can't send message to queue\n");
			msgctl(msqid,IPC_RMID,(struct msqid_ds*)NULL);
			exit(-1);
		}
		printf("Hello first!\n");
		i=0;
		while(i!=1) {
			printf("hello again!\n");
			maxlen=sizeof(mybuf);
			printf("first PID is %d\n",getpid());
			if((len=msgrcv(msqid,(struct msgbuf*)&mybuf,maxlen,getpid(),0))<0) {
				printf("Error: can't recive message from queue\n");
				exit(-1);
			}
			printf("message type = %ld, ID is %d message %s\n",mybuf.mtype,mybuf.id,mybuf.message);
		i++;
		printf("good bye\n");
		}
	} else {
		mybuf.mtype=1;
		mybuf.id=getpid();
		len=sizeof(mybuf);
		if(msgsnd(msqid,(struct msgbuf*)&mybuf,len,0)<0) {
			printf("Error: Can't send message to queue\n");
			msgctl(msqid,IPC_RMID,(struct msqid_ds*)NULL);
			exit(-1);
		}
		printf("Hello second!\n");
		i=0;
		while(i!=1) {
			printf("hello again!\n");
			maxlen=sizeof(mybuf);
			printf("second PID is %d\n",getpid());
			if((len=msgrcv(msqid,(struct msgbuf*)&mybuf,maxlen,getpid(),0))<0) {
				printf("Error: can't recive message from queue\n");
				exit(-1);
			}
			printf("message type = %ld, ID is %d message %s\n",mybuf.mtype,mybuf.id,mybuf.message);
		i++;
		}
	}
}

