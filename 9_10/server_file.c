#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#define LAST_MESSAGE 255

struct mymsgbuf
{
	long mtype;
	int id;
	char message[100];
	
}mybuf;

int main() {
	int len,maxlen,msqid,i=0;
	char pathname[]="client.c";
	key_t key;
	if((key=ftok(pathname,0))<0) {
		printf("Error: can't generate key\n");
		exit(-1);
	}
	if((msqid=msgget(key, 0666 | IPC_CREAT))<0) {
		printf("Error: can't get msqid\n");
		exit(-1);
	}
	while(1) {
		maxlen=sizeof(mybuf);
		printf("max len is %d\n",maxlen);
		if((len=msgrcv(msqid,(struct msgbuf*)&mybuf,maxlen,1,0))<0) {
			printf("Error: can't recive message from queue\n");
			exit (-1);
		}
		printf("message type = %ld, ID is %d\n",mybuf.mtype,mybuf.id);
		mybuf.mtype=mybuf.id;
		printf("Mybuf.mtype is %ld\n",mybuf.mtype);
		len=sizeof(mybuf);
		strcpy(mybuf.message,"Hello client. This is message from server.\n");
		printf("len is %d\n",len);
		if(msgsnd(msqid,(struct msgbuf*)&mybuf,len,0)<0) {
			printf("Error: Can't send message to queue\n");
			msgctl(msqid,IPC_RMID,(struct msqid_ds*)NULL);
			exit(-1);
		}
		printf("Hello!\n");
	}
}
