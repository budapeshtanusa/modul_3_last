#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <stdlib.h>

int main(){
	int i;
	int fd[2],result,semid;
	char string[14]="This is text.",restring[14],pathname[]="programm.c",str[12]="Second text",str2[12];
	key_t key;
	struct sembuf mybuf;
	if((key==ftok(pathname,0))<0)
	{
		printf("Error: can't generate key\n");
		exit(-1);
	}
	if((semid=semget(key,1,0666 | IPC_CREAT))<0)
	{
		printf("Error: can't get semid\n");
		exit(-1);
	}
	if(pipe(fd)<0)
	{
		printf("Error: Can't create pipe\n");
		exit(-1);
	}
	mybuf.sem_op=0;
	mybuf.sem_flg=0;
	mybuf.sem_num=0;
	result=fork();
	if(result<0)
	{
		printf("Error: Can't fork child\n");
		exit(-1);
	}
	else if(result>0)
	{
		mybuf.sem_op=1;
		if(semop(semid,&mybuf,1)<0)
		{
			printf("Error: can't synchronized write\n");
			exit(-1);
		}
		for(i=0;i<1000000000L;i++);
		write(fd[1],string,14);
		close(fd[1]);
		mybuf.sem_op=-1;
		if(semop(semid,&mybuf,1)<0)
		{
			printf("Error: can't synchronized write\n");
			exit(-1);
		}
		read(fd[0],str2,12);
		printf("Read from pipe: %s\n",str2);
		close(fd[0]);
	}
	else
	{
		mybuf.sem_op=-1;
		if(semop(semid,&mybuf,1)<0)
		{
			printf("Error: can't synchronized read\n");
			exit(-1);
		}
		read(fd[0],restring,14);
		printf("Read from pipe: %s\n",restring);
		close(fd[0]);
		for(i=0;i<1000000000L;i++);
		mybuf.sem_op=1;
		if(semop(semid,&mybuf,1)<0)
		{
			printf("Error: can't synchronized write\n");
			exit(-1);
		}
		write(fd[1],str,12);
		close(fd[1]);
	}

}
